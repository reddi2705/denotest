import { serve } from "https://deno.land/std/http/server.ts";
import Answer from "./src/Answer.ts";

const s = serve({ port: 8000 });

console.log("http://localhost:8000/");
const m = new Answer();
console.log(`Universal answer is 42: ${m.init(42)}`);

for await (const req of s) {
    req.respond({
        body: '<!doctype html>\n' +
            '\n' +
            '<html lang="en">\n' +
            '<head>\n' +
            '  <meta charset="utf-8">\n' +
            '\n' +
            '  <title>Deno test</title>\n' +
            '  <meta name="description" content="Deno test">\n' +
            '  <meta name="author" content="JC">\n' +
            '</head>\n' +
            '\n' +
            '<body>\n' +
            '  <div class="Deno">\n' +
            '    Welcome to Deno 🦕\n' +
            '  </div>\n' +
            '</body>\n' +
            '</html>'
    });
}
