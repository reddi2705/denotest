import BaseAnswer from "./BaseAnswer";

export default class Answer implements BaseAnswer {
    init(variable: number): boolean {
        return variable === 42;
    }
}
