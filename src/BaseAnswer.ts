export default interface BaseAnswer {
    init(variable: number): boolean;
}
